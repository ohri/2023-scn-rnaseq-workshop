# 2023 Stem Cell Network RNA-seq Workshop

## Workshop Dates 
* Session 1: May 11, 12 Tutorial Day May 15, 11-5PM (EDT)
* Session 2: May 23, 24 Tutorial Day May 29, 11-5PM (EDT)

## Workshop Venue
A Zoom meeting link for each session will be sent to participants separately.

## Organizers		
Dr. Bill Stanford (uOttawa/OHRI)  
Dr. Ted Perkins (uOttawa/OHRI)  

## Workshop Files

* [Workshop Agenda](Files/2023_RNA-seq_Analysis_Workshop_Agenda.pdf) 

### R Language Introduction

* Please complete [this one hour](Files/rlangintro.md) online class before your scheduled workshop date if you are not an experienced R programmer


### RNASeq Workshop
_Content will be linked below over the course as it becomes available_

* [Installation instructions for required R packages](Files/R_package_installation.txt)

### Day 1:

Overview and Intro (Dr. Theodore Perkins)
* Session 1: [PowerPoint](Files/RNAseq_workshop_20230510_PerkinsAM.pptx), [PDF](Files/RNAseq_workshop_20230510_PerkinsAM.pdf), [Video](http://dropbox.ogic.ca/workshop_2023/2023session1Day1PerkinsIntro.mp4)
* Session 2: [PowerPoint](Files/2023_Workshop_Lecture_1_RNASeq_Basics.pptx), [PDF](Files/RNAseq_workshop_20230523_PerkinsAM.pdf)


[RNA-seq Basics (Gareth Palidwor)](Files/2023_Workshop_Lecture_1_RNASeq_Basics.pptx) [\[Video\]](http://dropbox.ogic.ca/workshop_2023/2023session1Day1Intro.mp4)
* Example multiqc file
    * [MultiQC report](http://www.ogic.ca/projects/workshop_2019/multiqc/multiqc_report.html?infile=false)

[Differential Gene Expression with DESeq2 (Christopher Porter)](Files/2023_Workshop_Lecture_2_DESeq2.pptx) [\[Video\]](http://dropbox.ogic.ca/workshop_2023/2023session1Day1DESeq2tutorial.mp4)
* [Analysis walk-through (R Markdown script)](Files/DESeq2_walkthrough.Rmd) [\[Video\]](http://dropbox.ogic.ca/workshop_2023/2023session1Day1DESeq2.mp4)
* [GEO page for downloading GSE50499_GEO_Ceman_counts.txt](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE50499)

[Gene Set Enrichment Analysis (Gareth Palidwor)](Files/2023_Workshop_Lecture_3.pptx) [\[Video\]](http://dropbox.ogic.ca/workshop_2023/2023session1Day2AnnotationEnrichment.mp4)
* [Exercise using g:Profiler](Files/2023_Workshop_Annotation_Enrichment_Exercise.pdf)
* [Files to use for exercise](Files/FilesForEnrichmentExercise.zip)

[RNA-seq Experimental Design  (Dr. Ted Perkins)](http://dropbox.ogic.ca/workshop_2023/RNASeqWorkshop_20230511_PerkinsPM.pdf) [\[Video\]](http://dropbox.ogic.ca/workshop_2023/2023session1PerkinsExperiment.mp4)



### Day 2:

[Introduction to the single-cell RNA sequencing workflow (Dr. David Cook)](Files/single_cell_workshop.pdf)   [\[Video starts at 44:20\]](http://dropbox.ogic.ca/workshop_2023/2023session1Day2AnnotationEnrichment.mp4)

[single-cell RNA sequencing analysis walk through Video](http://dropbox.ogic.ca/workshop_2023/2023session1Day2Tutorial.mp4)

[Notebooks and data](https://github.com/dpcook/scrna_seq_workshop_2023)
